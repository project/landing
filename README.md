# Landing

Paragraphs collection.

## Table
```
___| Title _______|_Name______|_title_|_short_|_body_|_image_|_gallery_|_opts_|_attach_|_video_|_slide_|_tab_|_Параграф__________
1  | Title-image  | title     | +++++ | +++++ |      | +++++ |         |      |        |       |       |     | Заголовок-картинка
2  | Text         | text      |       |       | ++++ |       |         |      |        |       |       |     | Текст
3  | Lead         | lead      |       | +++++ |      |       |         |      |        |       |       |     | Лид
4  | Сard         | card      | +++++ | +++++ |      | +++++ |         |      |        |       |       |     | Карточка
5  | Image        | image     |       |       |      | +++++ |         |      |        |       |       |     | Картинка
6  | Image text   | imgtext   |       |       | ++++ | +++++ |         | left/right    |       |       |     | Картинка с текстом
7  | Gallery      | gallery   | +++++ |       |      |       | +++++++ | grid/carousel |       |       |     | Галерея
8  | Text slider  | slider    | +++++ |       |      |       |         |      |        |       | +++++ |     | Слайдер
9  | Accordion    | accordion | +++++ |       |      |       |         | accordion/tabs|       |       | +++ | Аккордеон
10 | Contact form | form      | +++++ | +++++ |      |       |         |      |        |       |       |     | Формы
11 | Files        | file      | +++++ | +++++ |      |       |         |      | ++++++ |       |       |     | Файлы
12 | Video        | video     | +++++ | +++++ |      |       |         |      |        | +++++ |       |     | Видео
   | -- Tab       | tab       | +++++ |       | ++++ |       |         |      |        |       |       |     | -- Таб
   | -- Slide     | slide     |       | +++++ |      | +++++ |         |      |        |       |       |     | -- Слайд
```
